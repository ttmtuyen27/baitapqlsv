function layThongTinTuForm() {
  var maSV = document.getElementById("txtMaSV").value;
  var tenSV = document.getElementById("txtTenSV").value;
  var emailSV = document.getElementById("txtEmail").value;
  var diemToan = document.getElementById("txtDiemToan").value * 1;
  var diemLy = document.getElementById("txtDiemLy").value * 1;
  var diemHoa = document.getElementById("txtDiemHoa").value * 1;
  var sinhVien = new SinhVien(maSV, tenSV, emailSV, diemToan, diemLy, diemHoa);
  return sinhVien;
}

function xuatThongTin(dssv) {
  var contentHTML = "";
  for (var i = 0; i < dssv.length; i++) {
    var sinhVien = dssv[i];
    var contentTrTag = `
      <tr>
        <td>${sinhVien.maSV}</td>
        <td>${sinhVien.tenSV}</td>
        <td>${sinhVien.emailSV}</td>
        <td>${sinhVien.tinhDiemTrungBinh()}</td>
        <td>
          <button
            class="btn btn-success"
            onclick="suaSinhVien('${sinhVien.maSV}')"
          >
            Sửa
          </button>
          <button
            class="btn btn-danger"
            onclick="xoaSinhVien('${sinhVien.maSV}')"
          >
            Xóa
          </button>
        </td>
      </tr>
    `;
    contentHTML += contentTrTag;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function xuatThongTinLenForm(sinhVien) {
  document.getElementById("txtMaSV").value = sinhVien.maSV;
  document.getElementById("txtTenSV").value = sinhVien.tenSV;
  document.getElementById("txtEmail").value = sinhVien.emailSV;
  document.getElementById("txtDiemToan").value = sinhVien.diemToan;
  document.getElementById("txtDiemLy").value = sinhVien.diemLy;
  document.getElementById("txtDiemHoa").value = sinhVien.diemHoa;
}
