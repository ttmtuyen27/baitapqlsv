function ValidatorSV() {
  this.kiemTraRong = function (idTarget, idError, message) {
    targetValue = document.getElementById(idTarget).value.trim();
    if (!targetValue) {
      document.getElementById(idError).innerText = message;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  };
  this.kiemTraIdHopLe = function (danhSachSinhVien, newSinhVien) {
    var index = danhSachSinhVien.findIndex(function (item) {
      return item.maSV == newSinhVien.maSV;
    });
    console.log(index);
    if (index != -1) {
      document.getElementById("spanMaSV").innerText =
        "Mã sinh viên không được trùng";
      return false;
    }
    document.getElementById("spanMaSV").innerText = "";
    return true;
  };
  this.kiemTraEmailHopLe = function (idTarget, idError) {
    var pattern =
      /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    var targetValue = document.getElementById(idTarget).value;
    if (pattern.test(targetValue)) {
      document.getElementById(idError).innerText = "";
      return true;
    }
    document.getElementById(idError).innerText = "Email không hợp lệ";
    return false;
  };
  this.kiemTraTenHopLe = function (idTarget, idError) {
    var pattern = /[a-zA-Z]+/;
    var targetValue = document.getElementById(idTarget).value;
    if (pattern.test(targetValue)) {
      document.getElementById(idError).innerText = "";
      return true;
    }
    document.getElementById(idError).innerText = "Tên không hợp lệ";
    return false;
  };
  this.kiemTraDiemHopLe = function (idTarget, idError) {
    var valueTarget = document.getElementById(idTarget).value;
    console.log(valueTarget);
    if (valueTarget < 0 || valueTarget > 10 || valueTarget == "") {
      document.getElementById(idError).innerText = "Điểm không hợp lệ";
      return false;
    }
    document.getElementById(idError).innerText = "";
    return true;
  };
}
