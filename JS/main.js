var danhSachSinhVien = [];
var validator = new ValidatorSV();
const DSSV_LOCALSTORAGE = "DSSV_LOCALSTORAGE";

const luuLocalStorage = function () {
  var dssvJson = JSON.stringify(danhSachSinhVien);
  localStorage.setItem(DSSV_LOCALSTORAGE, dssvJson);
};

const timKiemViTri = function (id) {
  return danhSachSinhVien.findIndex(function (item) {
    return item.maSV == id;
  });
};

const kiemTraSinhVienHopLe = function (newSinhVien) {
  var isValidId =
    validator.kiemTraRong(
      "txtMaSV",
      "spanMaSV",
      "Mã sinh viên không được rỗng"
    ) && validator.kiemTraIdHopLe(danhSachSinhVien, newSinhVien);

  var isValidTen =
    validator.kiemTraRong(
      "txtTenSV",
      "spanTenSV",
      "Tên sinh viên không được rỗng"
    ) && validator.kiemTraTenHopLe("txtTenSV", "spanTenSV");
  var isValidEmail = validator.kiemTraEmailHopLe("txtEmail", "spanEmailSV");

  var isValidDiem =
    validator.kiemTraDiemHopLe("txtDiemToan", "spanToan") &
    validator.kiemTraDiemHopLe("txtDiemLy", "spanLy") &
    validator.kiemTraDiemHopLe("txtDiemHoa", "spanHoa");
  var isValid = isValidEmail && isValidId && isValidTen && isValidDiem;
  return isValid;
};

var dssvJson = localStorage.getItem(DSSV_LOCALSTORAGE);
if (dssvJson) {
  var danhSachSinhVien = JSON.parse(dssvJson);
  danhSachSinhVien = danhSachSinhVien.map(function (item) {
    return new SinhVien(
      item.maSV,
      item.tenSV,
      item.emailSV,
      item.diemToan,
      item.diemLy,
      item.diemHoa
    );
  });
  xuatThongTin(danhSachSinhVien);
}

function themSinhVien() {
  var newSinhVien = layThongTinTuForm();
  var isValid = kiemTraSinhVienHopLe(newSinhVien);
  if (isValid) {
    danhSachSinhVien.push(newSinhVien);
    xuatThongTin(danhSachSinhVien);
  }
  luuLocalStorage();
}

function xoaSinhVien(id) {
  var viTri = timKiemViTri(id);
  danhSachSinhVien.splice(viTri, 1);
  luuLocalStorage();
  xuatThongTin(danhSachSinhVien);
}

function suaSinhVien(id) {
  var viTri = timKiemViTri(id);
  var sinhVien = danhSachSinhVien[viTri];
  document.getElementById("txtMaSV").disabled = true;
  xuatThongTinLenForm(sinhVien);
}

function capNhatSinhVien() {
  var editSinhVien = layThongTinTuForm();
  var isValidTen = validator.kiemTraTenHopLe("txtTenSV", "spanTenSV");
  var isValid = true;
  var isValidDiem =
    validator.kiemTraDiemHopLe("txtDiemToan", "spanToan") &
    validator.kiemTraDiemHopLe("txtDiemLy", "spanLy") &
    validator.kiemTraDiemHopLe("txtDiemHoa", "spanHoa");
  var isValidEmail = validator.kiemTraEmailHopLe("txtEmail", "spanEmailSV");
  isValid = isValidDiem && isValidEmail && isValidTen;
  if (isValid) {
    var viTri = timKiemViTri(editSinhVien.maSV);
    danhSachSinhVien[viTri] = editSinhVien;
    luuLocalStorage();
    xuatThongTin(danhSachSinhVien);
  }
}

function resetSinhVien() {
  document.getElementById("txtMaSV").disabled = false;
  document.getElementById("spanMaSV").innerText = "";
  document.getElementById("spanTenSV").innerText = "";
  document.getElementById("txtMaSV").value = "";
  document.getElementById("txtTenSV").value = "";
  document.getElementById("txtEmail").value = "";
  document.getElementById("txtDiemToan").value = "";
  document.getElementById("txtDiemLy").value = "";
  document.getElementById("txtDiemHoa").value = "";
}

function timKiemSinhVien() {
  var tenSvCanTim = document.getElementById("txtSearch").value;
  var sinhVienCanTim = [];
  danhSachSinhVien.forEach(function (item) {
    if (item.tenSV == tenSvCanTim) sinhVienCanTim.push(item);
  });
  xuatThongTin(sinhVienCanTim);
}
